import re
def check_password(password):
    pattern_1 = re.compile('[A-Za-z0-9]+')
    text_1 = pattern_1.findall(password)
    pattern_2 = re.compile('[0-9]+')
    text_2 = pattern_2.findall(password)

# первым условием проверяем пароль на длину и наличие повторяющихся символов,
# вторым и третьим условиями проверяем пароль на наличие хотя бы одной буквы верхнего и нижнего регистра соответственно
# четвертым условием проверяем пароль на содержание в нем только латинских букв ицифр
# пятым условием проверяем пароль на наличие хотя бы одной цифры
    if (len(set(password)) == len(password) >= 7) & (password.islower() == False) & \
            (password.isupper() == False) & (text_1[0] == password) & (text_2 != []):
        return 'good_password'
    else:
        return 'bad_password'
